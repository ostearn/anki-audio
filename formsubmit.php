<?php

    function create_zip($files = array(), $dest = '', $overwrite = false) {
    if (file_exists($dest) && !$overwrite) {
        return false;
    }
    if (($files)) {
        $zip = new ZipArchive();
        if ($zip->open($dest, $overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
            return false;
        }
        foreach ($files as $file) {
            $zip->addFile($file, $file);
        }
        $zip->close();
        return file_exists($dest);
    } else {
        return false;
    }
}

function addzip($source, $destination) {
    $files_to_zip = glob($source . '/*');
    create_zip($files_to_zip, $destination);
}


if($_REQUEST["deckname"]){

	$deckname = $_REQUEST["deckname"];
	$invalid_characters = array("$", "%", "#", "<", ">", "|");
	$deckname = str_replace($invalid_characters, "", $deckname);

	$cardside = $_REQUEST["cardside"];
	
	$deckcontent = $_REQUEST["deckcontent"];
	$deckcontent = str_replace("&nbsp;", "", $deckcontent);

	mkdir($deckname);

	$newfile = "";

	foreach(preg_split("/((\r?\n)|(\r\n?))/", $deckcontent) as $line){
		
		if($line != ""){
			$linet = explode("	", $line);
			//echo $pieces[0]; 
			$front = @$linet[0]; 
			
			$back = @$linet[1]; 
		
			if($cardside == "Front"){
				$fronturl = str_replace(" ", "%20", $front);
				file_put_contents($deckname . "/" . $front . ".mp3", file_get_contents("http://responsivevoice.org/responsivevoice/getvoice.php?t=" . $fronturl . "&tl=id"));
				$newfile .= $front . " [sound:" . $front . ".mp3]" . "	" . $back . "\r\n";
			} else{
				$backurl = str_replace(" ", "%20", $back);
				file_put_contents($deckname . "/" . $back . ".mp3", file_get_contents("http://responsivevoice.org/responsivevoice/getvoice.php?t=" . $backurl . "&tl=id"));
				$newfile .= $front . "	" . $back . " [sound:" . $back . ".mp3]\r\n";
			}
		
	}
	} 
$filename = __DIR__ . "/" . $deckname . "/" . $deckname . ".txt";
file_put_contents($filename, $newfile);
//echo $deckname;

addzip ($deckname, $deckname . ".zip");
echo $deckname . ".zip";

}

// file_put_contents("test.mp3", file_get_contents("http://responsivevoice.org/responsivevoice/getvoice.php?t=Type%20text%20here&tl=id"));
?>