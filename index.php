<?php


// file_put_contents("test.mp3", file_get_contents("http://responsivevoice.org/responsivevoice/getvoice.php?t=Type%20text%20here&tl=id"));
?>

<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Anki Audio Decks</title>

	<link rel="stylesheet" href="assets/demo.css">
	<link rel="stylesheet" href="assets/form-basic.css">
	<script
			  src="https://code.jquery.com/jquery-1.12.4.min.js"
			  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
			  crossorigin="anonymous"></script>
	<script src="assets/dropzone.js"></script>
	<script>
		$fileupload = false;
		function submitForm(){

			var name = $('#deckname').val();
			// var name = 'sdfsdf';
			var side = $('#side').val();
			var content = $('#content').val();
			$.ajax({
				  url: "formsubmit.php",
				  data: {
				    deckname: name,
				    cardside: side,
				    deckcontent: content

				  },
				  success: function( data ) {
				    // $( "#weather-temp" ).html( "<strong>" + result + "</strong> degrees" );
				    // alert(data);
				    window.open( data );
				  }
			});
	}
	</script>

</head>


    <div class="main-content">

        <!-- You only need this form and the form-basic.css -->

        <form class="form-basic">

            <div class="form-title-row">
                <h1>Adding Indonesian Audio to your Anki Deck</h1>
            </div>
            <div>
                <p style="text-align: left; font-size: 0.8em;">AwesomeTTS is a fantastic Anki plugin which adds audio to your anki decks. Unfortunately, at the time of writing it does not provide a service which supports Bahasa Indonesia. This is due to Google adding a captcha to their translate TTS service. The developers of awesomeTTS are aware of this, and have added adding a service which supports Indonesian to their list of jobs. The service I have Suggested is ResponsiveVoice which I have used here and may even be better than Google Translate. Feel free to use this tool which supports simple deck types in the meantime.</p><br />
                <h3 style="text-align: left;">Steps</h2><br />
                <p style="text-align: left; font-size: 0.8em;">1. Export your Anki Deck as a .txt file</p>
                <p style="text-align: left; font-size: 0.8em;">2. Select a unique deck name below (this is important)</p>
                <p style="text-align: left; font-size: 0.8em;">3. Either paste the contents of your txt file, or drag and drop it below.</p>
                <p style="text-align: left; font-size: 0.8em;">4. You will be given the option to download a zip file containing both a new txt file to import back into Anki, and the .mp3 files which should be pasted into your media collection (by default documents/anki/user1/collection.media).</p><br />
                <p style="text-align: left; font-size: 0.8em;">You can download an example file <a href="window.open(example.txt)">here</a></p><br/>
            </div>            
            <div class="form-row">
                <label>
                    <span>Deck Name</span>
                    <input type="text" id="deckname"></input>

                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Which side of the card do you want Audio Added to?</span>
                    <select id="side">
                        <option>Back</option>
                        <option>Front</option>
                    </select>
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Please paste the contents of your Anki Deck Export Here (or upoad the file below)</span>
                    <textarea id="content"></textarea>
                </label>
            </div>

	</form>

	<form class="form-basic">
		<div id="my-awesome-dropzone" class="dropzone" style="max-width: 500px;"></div>
	</form>
	<form class="form-basic">
        <div class="form-row">
        <button href="#" onclick="submitForm();">Submit Form</button>
    	</div>
	</form>

    </div>




<script>

$("#my-awesome-dropzone").dropzone({
url: "upload-files.php", 
maxFilesize: 2,
acceptedFiles: ".txt",
sending: function (file, xhr, formData) {
var sider = $("#side").val();
var deckName = $("#deckname").val();
formData.append("filename", deckName);
formData.append("side", sider);
},
success: function( something, data ) {
// $( "#weather-temp" ).html( "<strong>" + result + "</strong> degrees" );
// alert(data);
window.open( data );
}
});
</script>
</body>
</html>
